import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyNavbarComponent } from './my-navbar/my-navbar.component';

export const routes: Routes = [
  {
    path: '',
    component: MyNavbarComponent
  }
];
